import math

class Punto:
    x=int
    y=int
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __str__(self):
        return "({}, {})".format(self.x, self.y)

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            print("{} A pertenece al primer cuadrante".format(self))
        elif self.x < 0 and self.y > 0:
            print("{}  pertenece al segundo cuadrante".format(self))
        elif self.x < 0 and self.y < 0:
            print("{} C pertenece al tercer cuadrante".format(self))
        elif self.x > 0 and self.y < 0:
            print("{}  pertenece al cuarto cuadrante".format(self))
        elif self.x != 0 and self.y == 0:
            print("{} se sitúa sobre el eje X".format(self))
        elif self.x == 0 and self.y != 0:
            print("{} se sitúa sobre el eje Y".format(self))
        else:
            print("{} D se encuentra sobre el origen".format(self))

    def vector(self, pun2):
        print("El vector entre {} y {} es ({}, {})".format(
            self, pun2, pun2.x - self.x, pun2.y - self.y) )

    def distancia(self, pun3):
        d = math.sqrt( (pun3.x - self.x)**2 + (pun3.y - self.y)**2 )
        print("La distancia entre los puntos {} y {} es {}".format(
            self, pun3, d))


class Rectangulo:
    punto_inicial:Punto
    punto_final:Punto
    def __init__(self, punto_inicial=Punto(), punto_final=Punto()):
        self.punto_inicial = punto_inicial
        self.punto_final = punto_final


        self.Base = abs(self.punto_final.x - self.punto_inicial.x)
        self.Altura = abs(self.punto_final.y - self.punto_inicial.y)
        self.Area = self.Base * self.Altura

    def base(self):
        print("La base del rectángulo es: {}".format( self.Base ) )

    def altura(self):
        print("La altura del rectángulo es: {}".format( self.Altura ) )

    def area(self):
        print("El área del rectángulo es: {}".format( self.Area ) )


A = Punto(2,3)
B = Punto(5,5)
C = Punto(-3, -1)
D = Punto(0,0)
print("A", A)
print("B", B)
print("C", C)
print("D", D)

A.cuadrante()
C.cuadrante()
D.cuadrante()

A.vector(B)
B.vector(A)

A.distancia(B)
B.distancia(A)

A.distancia(D)
B.distancia(D)
C.distancia(D)

R = Rectangulo(A, B)
R.base()
R.altura()
R.area()